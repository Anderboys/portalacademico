package com.example.ander.portalacademico_35;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {
    EditText txtcod,txtpas;
    Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


        txtcod=(EditText)findViewById(R.id.txtcod);
        txtpas=(EditText)findViewById(R.id.txtpas);
        btnlogin=(Button)findViewById(R.id.btningresar);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String cod = txtcod.getText().toString();
                final String pas = txtpas.getText().toString();

                Thread tr = new Thread(){
                    @Override
                    public void run(){    // creamos metodo run clip derecho Generate - Override Methods - Run()Void
                        final String resultado=loginAlumno(cod,pas);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                int r= obtDatosJSON(resultado);
                                if(r>0) {
                                    Intent intent = new Intent(getApplicationContext(), Principal.class);
                                    // TODO enviar extra
                                    intent.putExtra("CodAlumno1",cod);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(Login.this, "Usuario o pas Incorrectos", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                };
                tr.start();




            }
        });
    }

    public String loginAlumno(String cod,String pass){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

//            url = new URL("http://192.168.0.6:8081/WSsistemaAcademico/LoginAlumno.php?cod="+cod+"&pas="+pass+"");
            url = new URL(" https://andercalcina.000webhostapp.com/WSsistemaAcademico/LoginAlumno.php?cod="+cod+"&pas="+pass+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }

    public int obtDatosJSON (String respuesta){
        int res=0;

        try {
            JSONArray json=new JSONArray(respuesta);
            if(json.length()>0){
                res=1;
            }

        }catch (Exception e){}

        return res;
    }


}
