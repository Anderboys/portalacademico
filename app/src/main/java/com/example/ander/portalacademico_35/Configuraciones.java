package com.example.ander.portalacademico_35;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import Beans.Alumno;
import androidx.appcompat.app.AppCompatActivity;

public class Configuraciones extends AppCompatActivity {


    TextView tvcodigo,tvalumno,tvenabled;
    EditText edtcorreo,edtpass;
    Button btnguardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuraciones);

        tvcodigo = (TextView)findViewById(R.id.tvcodigo2);
        tvalumno = (TextView)findViewById(R.id.tvalumno2);
        tvenabled = (TextView)findViewById(R.id.tvenabled2);
        edtcorreo = (EditText)findViewById(R.id.edtcorreo2);
        edtpass = (EditText)findViewById(R.id.edtpass2);
        btnguardar = (Button)findViewById(R.id.btnguardar);

        // TODO recibe extra COD usuario
        Bundle recibe = this.getIntent().getExtras();

        if(recibe!=null){
            String codAlumno = recibe.getString("CodAlumno2");
            tvcodigo.setText(codAlumno);
        }


        cargardatosAlumno();

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              final String email = edtcorreo.getText().toString();
              final String password = edtpass.getText().toString();
              final String codalumno = tvcodigo.getText().toString();

                Thread tr = new Thread(){

                    public void run(){
                        SendPutEditarUsu(email,password,codalumno);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Configuraciones.this, "Editado Correctamente", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                };
                tr.start();

                Intent intent = new Intent(getApplicationContext(),Principal.class);

//                String codUsu = tvcodigo.getText().toString();
                // TODO enviar extra COD usuario a Principal.java
                intent.putExtra("CodAlumno3",codalumno);

                startActivity(intent);

            }
        });

    }
    //-------------TODO  METODO UPDATE ALUMNO  -------------
    public String SendPutEditarUsu(String email, String pass, String codalum){

        URL url=null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;

        try{

//            url = new URL("http://192.168.0.6:8081/WSsistemaAcademico/UpdateAlumno.php?email="+email+"&pas="+pass+"&cod="+codalum+" ");
            url = new URL(" https://andercalcina.000webhostapp.com/WSsistemaAcademico/UpdateAlumno.php?email="+email+"&pas="+pass+"&cod="+codalum+" ");
            HttpURLConnection conection=(HttpURLConnection)url.openConnection();
            respuesta=conection.getResponseCode();

            resul=new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in=new BufferedInputStream(conection.getInputStream());
                BufferedReader reader=new BufferedReader(new InputStreamReader(in));

                while((linea=reader.readLine())!=null){
                    resul.append(linea);
                }
            }
        }catch(Exception e){}

        return resul.toString();
    }
    //----------------------------------------------------------------------------------------------





    //-------------  METODO PARA CARGAR LISTVIEW POR COD-ALUMNO Y DESCRIPCION DE CICLO  -------------

    public String listardatos(String cod){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

//            url = new URL("http://192.168.0.6:8081/wssistemaacademico/LitarAlumno.php?cod="+cod+"");
            url = new URL(" https://andercalcina.000webhostapp.com/WSsistemaAcademico/LitarAlumno.php?cod="+cod+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }

    public ArrayList<Alumno> obtDatosJSON(String response){

        ArrayList<Alumno> listado = new ArrayList<Alumno>();

        try{
            JSONArray json = new JSONArray(response);

            for(int i=0;i<json.length();i++){

                Alumno listarNotas= new Alumno(

                        json.getJSONObject(i).getString("codAlum"),
                        json.getJSONObject(i).getString("nomAlum"),
                        json.getJSONObject(i).getInt("Enabled"),
                        json.getJSONObject(i).getString("emailAlum"),    // campos - tabla usuarios de la DB dbforandroid
                        json.getJSONObject(i).getString("PasAlum")
                );


                listado.add(listarNotas);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e+"", Toast.LENGTH_SHORT).show();
        }
        return listado;
    }


    public ArrayList<Alumno> obtDatosJSON2(String response) {
        ArrayList<Alumno> listado = new ArrayList<Alumno>();
        try {
            JSONArray json = new JSONArray(response);

            for (int i = 0; i < json.length(); i++) {

                Alumno listarAlumno= new Alumno(

                        json.getJSONObject(i).getString("codAlum"),
                        json.getJSONObject(i).getString("nomAlum"),
                        json.getJSONObject(i).getInt("Enabled"),
                        json.getJSONObject(i).getString("emailAlum"),    // campos - tabla usuarios de la DB dbforandroid
                        json.getJSONObject(i).getString("PasAlum")

                );

                tvcodigo.setText(listarAlumno.getCod());
                tvalumno.setText(listarAlumno.getNombre());
                int habilitado = listarAlumno.getEnabled();
                if(habilitado>0){
                    tvenabled.setText("Habilitado");
                }else{
                    tvenabled.setText("Inhabilitado");
                }
                edtcorreo.setText(listarAlumno.getEmail());
                edtpass.setText(listarAlumno.getPas());

//                tvcodigo2.setText(json.getJSONObject(i).getString("codAlum"));
//                tvalumno.setText(json.getJSONObject(i).getString("codAlum"));
//                tvenabled.setText(json.getJSONObject(i).getString("codAlum"));
//                edtcorreo2.setText(json.getJSONObject(i).getString("codAlum"));
//                edtpass2.setText(json.getJSONObject(i).getString("codAlum"));

                listado.add(listarAlumno);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error: "
                    + e, Toast.LENGTH_LONG).show();
        }
        return listado;
    }
    //--------------------------------------------------------------------------------------------




    // ----------------------------------------------------------------------
    private void cargardatosAlumno(){

        final String Codusuario = tvcodigo.getText().toString();

        Thread tr = new Thread(){
            @Override
            public void run(){    // creamos metodo run clip derecho Generate - Override Methods - Run()Void
                final String resultado=listardatos(Codusuario);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        obtDatosJSON2(resultado);
                    }
                });
            }
        };
        tr.start();

    }
//-------------------------------------------------------------------------



    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.noGuardar) {
            this.finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    //----------------------------------------------------------------












}
