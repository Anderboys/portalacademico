package com.example.ander.portalacademico_35;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import Beans.ListarNotas;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Principal extends AppCompatActivity {

    TextView TVCodalumno;
    Spinner spciclo;

    RecyclerView recyclerview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);

        TVCodalumno = (TextView) findViewById(R.id.TValumno);
        spciclo = (Spinner) findViewById(R.id.spciclo);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerpersona);


        // TODO recibe extra COD usuario de Login.java
           Bundle recibe = this.getIntent().getExtras();
           String codAlumno = recibe.getString("CodAlumno1");
           TVCodalumno.setText(codAlumno);

        // TODO recibe extra COD usuario de Configuraciones.java
        if(codAlumno ==null){
            String codAlumno2 = recibe.getString("CodAlumno3");
            TVCodalumno.setText(codAlumno2);
        }



        listarCiclosALL();


        //---------------------------------------------------------


        spciclo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                listarRegistro();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        btnlogout.setOnClickListener(new View.OnClickListener()
//
//        {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(getApplicationContext(), Login.class);
//                startActivity(intent);
//            }
//        });

        //---------------------------------------------------------------


    }
    //---------------------------  METODO PARA CARGAR SPINNER CICLOS  listarCiclos= A0001 ----------------------
    public String listarCiclos(String cod){
        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{
//            url = new URL("http://192.168.0.6:8081/WSsistemaAcademico/listarCiclos.php?cod="+cod+"");
            url = new URL(" https://andercalcina.000webhostapp.com/WSsistemaAcademico/listarCiclos.php?cod="+cod+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }
        return resul.toString();
    };

    //1
    public ArrayList<String> obtDatosJSON2(String response){
        ArrayList<String> listado = new ArrayList<String>();
        try{
            JSONArray json = new JSONArray(response);
            String texto="";
            for(int i=0;i<json.length();i++){
                texto = json.getJSONObject(i).getString("descripcion");
                listado.add(texto);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e+"", Toast.LENGTH_SHORT).show();
        }
        return listado;
    }

    //2
    public  void cargaListado2(ArrayList<String> datos){
        ArrayAdapter<String> adaptador= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos);
        Spinner listado = (Spinner)findViewById(R.id.spciclo);
        listado.setAdapter(adaptador);
    }
//----------------------------------------------------------------------------------


    //--------------- TODO MAIN listarCiclosALL  ------------------------------------------------------
    public void listarCiclosALL(){
        // METODOS PARA CARGAR Spinner CICLOS DESDE EL LOAD

        final String cod = TVCodalumno.getText().toString();
        Thread tr = new Thread() {
            @Override
            public void run() {

                final String resultado = listarCiclos(cod);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cargaListado2(obtDatosJSON2(resultado));
                    }
                });
            }
        };
        tr.start();
    }
    //------------------------------------------------------------------------------------------------------



    //-------------  METODO PARA CARGAR LISTVIEW POR COD-ALUMNO Y DESCRIPCION DE CICLO  -------------

    public String listardatos(String cod,String descripcion){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

//            url = new URL("http://192.168.0.6:8081/WSsistemaAcademico/ListarNotas.php?cod="+cod+"&descripcion="+descripcion+"");
            url = new URL(" https://andercalcina.000webhostapp.com/WSsistemaAcademico/ListarNotas.php?cod="+cod+"&descripcion="+descripcion+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }


    public ArrayList<ListarNotas> obtDatosJSON(String response){

        ArrayList<ListarNotas> listado = new ArrayList<ListarNotas>();

        try{
            JSONArray json = new JSONArray(response);

            for(int i=0;i<json.length();i++){

                ListarNotas listarNotas= new ListarNotas(
                        json.getJSONObject(i).getString("codCurso"),
                        json.getJSONObject(i).getString("nomCurso"),
                        json.getJSONObject(i).getString("promedio"),    // campos - tabla usuarios de la DB dbforandroid
                        json.getJSONObject(i).getString("descripcion"));


                listado.add(listarNotas);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e+"", Toast.LENGTH_SHORT).show();
        }
        return listado;
    }


    public  void cargaListado(ArrayList<ListarNotas> datos){
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        AdapterListarNotas adapterPersonas = new AdapterListarNotas(datos,this);
        recyclerview.setAdapter(adapterPersonas);

    }

    //--------------------------------------------------------------------------------------------



    // CARGAR LOS 3 METODOS  listardatos -> cargaListado ( obtDatosJSON )
    //--------------------------------------------------------------------------------------------
    public void listarRegistro(){

        final String cod = TVCodalumno.getText().toString();
        final String descripcion = spciclo.getSelectedItem().toString();

        Thread tr = new Thread(){
            @Override
            public void run(){    // creamos metodo run clip derecho Generate - Override Methods - Run()Void
                final String resultado=listardatos(cod,descripcion);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cargaListado(obtDatosJSON(resultado));
                    }
                });
            }
        };
        tr.start();

    }
    //--------------------------------------------------------------------------------------------



    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {



            case R.id.configuration:

                // TODO EXTRAS
                Intent intent2 = new Intent(getApplicationContext(),Configuraciones.class);

                String usuario = TVCodalumno.getText().toString();
                intent2.putExtra("CodAlumno2",usuario);

                startActivity(intent2);
                return true;

            case R.id.exit:

                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //----------------------------------------------------------------


}
