package com.example.ander.portalacademico_35;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import Beans.ListarNotas;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
                                    // 0.   extends RecyclerView.Adapter<AdapterListarNotas.ViewHolderListarNotas>{
public class AdapterListarNotas extends RecyclerView.Adapter<AdapterListarNotas.ViewHolderListarNotas>{

            //datos
            ArrayList<ListarNotas> listaNOTAS  = new ArrayList<ListarNotas>();
            //7.
            Context context;

            //8. agregar , Context context
            public AdapterListarNotas(ArrayList<ListarNotas> listausuarios , Context context){

                this.listaNOTAS = listausuarios;
                //9.
                this.context=context;
            }

    @NonNull
    @Override
    public ViewHolderListarNotas onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlistarnotas,parent,false);

        return new ViewHolderListarNotas(view,context,listaNOTAS);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderListarNotas holder, int position) {

        holder.codCurso.setText(listaNOTAS.get(position).getCodCurso());
        holder.nomCurso.setText(listaNOTAS.get(position).getNomCurso());
        holder.promedio.setText(listaNOTAS.get(position).getPromedio());
        holder.descripcion.setText(listaNOTAS.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return listaNOTAS.size();
    }

    public class ViewHolderListarNotas extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView codCurso,nomCurso,promedio,descripcion;

        //2. crear ArrayList de clase Persona y Context
        ArrayList<ListarNotas> listarNotas = new ArrayList<ListarNotas>();
        Context ctx;

     public ViewHolderListarNotas(View vista,Context ctx,ArrayList<ListarNotas> listarNotas) {
        super(vista);

        //4.
        this.listarNotas = listarNotas;
        this.ctx=ctx;

        //5. AGREGAR  itemView.setOnClickListener(this);
        itemView.setOnClickListener(this);

         codCurso = (TextView)vista.findViewById(R.id.txtcodCurso);
         nomCurso = (TextView)vista.findViewById(R.id.txtnomCurso);
         promedio = (TextView)vista.findViewById(R.id.txtpromedio);
         descripcion = (TextView)vista.findViewById(R.id.txtdescripcion);

    }

    // POR ITEM CLICK
        @Override
        public void onClick(View view) {

//            int position = getAdapterPosition();
//            ListarNotas persona = this.personas.get(position);
//            Intent intent = new Intent(this.ctx,DetallesPersona.class);
//
//            intent.putExtra("ID",persona.getId());
//            intent.putExtra("NOMBRE",persona.getNom());
//            intent.putExtra("APELLIDO",persona.getApe());
//            intent.putExtra("DISTRITO",persona.getDireccion());
//            intent.putExtra("TELEFONO",persona.getTelefono());
//            intent.putExtra("ESTADO",persona.getEstado());
//            intent.putExtra("DNI",persona.getDni());
//            intent.putExtra("FECHANACI",persona.getFechanaci());
//
//            this.ctx.startActivity(intent);


        }
    }

}
