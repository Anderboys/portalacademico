package Beans;

public class Alumno {

    private String cod;
    private String nombre;
    private int enabled;
    private String email;
    private String pas;

    public Alumno(String cod, String nombre, int enabled, String email, String pas) {
        this.cod = cod;
        this.nombre = nombre;
        this.enabled = enabled;
        this.email = email;
        this.pas = pas;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPas() {
        return pas;
    }

    public void setPas(String pas) {
        this.pas = pas;
    }
}
