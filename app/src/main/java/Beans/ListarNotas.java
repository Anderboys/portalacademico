package Beans;

public class ListarNotas {

    private String codCurso;
    private String nomCurso;
    private String promedio;
    private String descripcion;

    public String getCodCurso() {
        return codCurso;
    }

    public void setCodCurso(String codCurso) {
        this.codCurso = codCurso;
    }

    public String getNomCurso() {
        return nomCurso;
    }

    public void setNomCurso(String nomCurso) {
        this.nomCurso = nomCurso;
    }

    public String getPromedio() {
        return promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ListarNotas(String codCurso, String nomCurso, String promedio, String descripcion) {
        this.codCurso = codCurso;
        this.nomCurso = nomCurso;
        this.promedio = promedio;
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {

        return
                "codCurso=" + codCurso + '\n' +
                "Curso=" + nomCurso + '\n' +
                "Promedio=" + promedio + '\n' +
                "Ciclo=" + descripcion
                ;

//        return  codCurso + " - " +
//                nomCurso + " - "+
//                promedio + " - " +
//                descripcion
//                ;
    }
}
